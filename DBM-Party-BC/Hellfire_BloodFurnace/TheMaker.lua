local mod = DBM:NewMod("TheMaker", "DBM-Party-BC", 4)
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 2250 $"):sub(12, -3))
mod:SetCreatureID(17381)
mod:SetZone()

mod:RegisterCombat("combat", 17381)

mod:RegisterEvents(
    "SPELL_CAST_SUCCESS"
)

local timerBomb                 = mod:NewCDTimer(10, 30925)
local timerMight                 = mod:NewCDTimer(30, 30923)
local warnMight                 = mod:NewTargetAnnounce(30923, 4)

function mod:OnCombatStart(delay)
    timerBomb:Start(5)
    timerMight:Start(20)
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(30925) then
        timerBomb:Start()
    elseif args:IsSpellID(30923) and args.sourceName == L.name then
        timerMight:Start()
        warnMight:Show(args.destName)
        if args:IsPlayer() then
           if self:IsWeaponDependent() then
                PickupInventoryItem(16)
                PutItemInBackpack()
                PickupInventoryItem(17)
                PutItemInBackpack()
            elseif select(2, UnitClass("player")) == "HUNTER" then
                PickupInventoryItem(18)
                PutItemInBackpack()
            end
        end
    end
end
