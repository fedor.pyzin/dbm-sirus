local mod = DBM:NewMod("ZulJin", "DBM-ZulAman")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4873 $"):sub(12, -3))

mod:SetCreatureID(23863)
mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell", L.YellKill)

mod:RegisterEvents(
    "SPELL_CAST_SUCCESS",
    "SPELL_DAMAGE",
    "SPELL_MISSED",
    "SPELL_DISPEL",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "CHAT_MSG_MONSTER_YELL",
    "UNIT_SPELLCAST_SUCCEEDED",
    "UNIT_HEALTH"
)

local warnNextPhaseSoon         = mod:NewAnnounce("WarnNextPhaseSoon", 1)
local warnNextPhase             = mod:NewAnnounce("WarnNextPhase", 2)
local warnWhirlwind             = mod:NewSpellAnnounce(17207, 2)
local warnThrow                 = mod:NewTargetAnnounce(43093, 3)
local warnParalysis             = mod:NewSpellAnnounce(43095, 2)
local warnJump                  = mod:NewTargetAnnounce(43153, 3)
local warnFlamePillar           = mod:NewTargetAnnounce(43216, 4)

local specWarnThrow             = mod:NewSpecialWarningYou(43093)
local specWarnFlamePillar       = mod:NewSpecialWarningMove(43216)
local specWarnFlamePillarNear   = mod:NewSpecialWarning("SpecWarnFlamePillarNear")

local yellFlamePillar           = mod:NewYell(43216)

local timerWhirlwindCD          = mod:NewCDTimer(15, 17207)
local timerThrowCD              = mod:NewCDTimer(10, 43093)
local timerParalysisCD          = mod:NewCDTimer(20, 43095)
local timerClawRageCD           = mod:NewCDTimer(23, 43150)
local timerJumpCD               = mod:NewCDTimer(20, 43153)
local timerBreathCD             = mod:NewCDTimer(10, 43215)
local timerFlameWhirlCD         = mod:NewCDTimer(12, 43213)
local timerFlamePillarCD        = mod:NewCDTimer(10, 43216)

local berserkTimer              = mod:NewBerserkTimer(600)

mod:AddSetIconOption("SetIconOnGrievousThrow", 43093 , {8}, true)
mod:AddSetIconOption("SetIconOnFlamePillar", 43216 , {8}, true)
mod:AddBoolOption("RangeFrame", true)
mod:AddBoolOption("PillarArrow", true)
mod:AddBoolOption("AchievementCheckWind", true, "announce")
mod:AddBoolOption("AchievementCheckDispel", false, "announce")

local phaseCounter = 1
local bleedTargets = {}
local bleedCounter = 0
local windTargets = {}
local windFail = true
local paralysisFail = true

function mod:OnCombatStart(delay)
    timerWhirlwindCD:Start(7-delay)
    timerThrowCD:Start(19-delay)
    berserkTimer:Start(-delay)
    phaseCounter = 1
    bleedCounter = 0
    table.wipe(bleedTargets)
    table.wipe(windTargets)
    windFail = true
    paralysisFail = true
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(17207) then
        warnWhirlwind:Show()
        timerWhirlwindCD:Start()
    elseif args:IsSpellID(43093) then
        self:PlaySound("boom_reload", "dmg", "vnoge", "mp_scratch")
        if self.Options.SetIconOnGrievousThrow then
            self:SetIcon(args.destName, 8)
        end
        if args:IsPlayer() then
            specWarnThrow:Show()
        else
            warnThrow:Show(args.destName)
        end
        timerThrowCD:Start()
    elseif args:IsSpellID(43095) then
        warnParalysis:Show()
        timerParalysisCD:Show()
    elseif args:IsSpellID(43215) then 
        timerBreathCD:Start()
        timerBreathCD:Schedule(10) -- backup
    elseif args:IsSpellID(43213) then
        timerFlameWhirlCD:Start()
    end
end

function mod:BleedTargets()
    warnJump:Show(table.concat(bleedTargets, "<, >"))
    bleedCounter = 0
    table.wipe(bleedTargets)
    timerJumpCD:Start()
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(43153) then
        bleedCounter = bleedCounter + 1
        if bleedCounter >= 9 then
            self:ScheduleMethod(0.1, "BleedTargets")
        end
    elseif args:IsSpellID(43137) and args:IsDestTypePlayer() and windFail and self.Options.AchievementCheckWind and DBM:GetRaidRank() > 0 then
        if self.Options.AchievementCheckWind then
            windTargets[args.destName] = windTargets[args.destName] and (windTargets[args.destName] + 1) or 1
            if windTargets[args.destName] > 1 then
                SendChatMessage(L.WindFail:format(GetAchievementLink(6144), args.destName), "RAID_WARNING")
                windFail = false
            end
        end
    end
end

function mod:SPELL_MISSED(args)
    if args:IsSpellID(43137) and args:IsDestTypePlayer() and args.absorbed > 0 and windFail and self.Options.AchievementCheckWind and DBM:GetRaidRank() > 0 then
        if self.Options.AchievementCheckWind then
            windTargets[args.destName] = windTargets[args.destName] and (windTargets[args.destName] + 1) or 1
            if windTargets[args.destName] > 1 then
                SendChatMessage(L.WindFail:format(GetAchievementLink(6144), args.destName), "RAID_WARNING")
                windFail = false
            end
        end
    end
end

function mod:SPELL_DISPEL(args)
    if args.extraSpellId == 43095 and args:IsDestTypePlayer() and paralysisFail and self.Options.AchievementCheckDispel and DBM:GetRaidRank() > 0 then
        SendChatMessage(L.ParalysisFail:format(GetAchievementLink(6145), args.sourceName), "RAID_WARNING")
        paralysisFail = false
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(43153) then
        if args:IsDestTypePlayer() then
            bleedTargets[#bleedTargets + 1] = args.destName
            self:PlaySound("sheep_scream")
        end
    elseif args:IsSpellID(43150) and self:AntiSpam(10, "claw") then
        timerClawRageCD:Start()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(43093) then
        if self.Options.SetIconOnGrievousThrow then
            self:RemoveIcon(args.destName)
        end
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellBear then
        warnNextPhase:Show(L.Bear, 9634)
        self:PlaySound("b_kakebnut")
        timerWhirlwindCD:Cancel()
        timerThrowCD:Cancel()
        timerParalysisCD:Start(7)
    elseif msg == L.YellHawk then
        warnNextPhase:Show(L.Hawk, 40120)
        timerParalysisCD:Cancel()
    elseif msg == L.YellLynx then
        warnNextPhase:Show(L.Lynx, 768)
        timerJumpCD:Start(14)
    elseif msg == L.YellDragon then
        warnNextPhase:Show(L.Dragon, 61846)
        if self.Options.RangeFrame then
            DBM.RangeCheck:Show(5)
        end
        timerClawRageCD:Cancel()
        timerJumpCD:Cancel()
        timerFlameWhirlCD:Start(5)
        timerBreathCD:Start(16)
        timerFlamePillarCD:Start(17)
        self:ScheduleMethod(17, "PillarTimer")
    end
end

function mod:PillarTarget()
    local target = self:GetBossTarget(23863)
    if target and mod:LatencyCheck() then
        self:SendSync("Pillar", target)
    end
end

function mod:UNIT_SPELLCAST_SUCCEEDED(uId, spellName)
    if spellName == GetSpellInfo(43216) then
        self:ScheduleMethod(0.1, "PillarTarget")
    end
end

function mod:UNIT_HEALTH(uId)
    if (self:GetUnitCreatureId(uId) == 23863 and phaseCounter == 1 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.82) then
        phaseCounter = phaseCounter + 1
        warnNextPhaseSoon:Show(L.Bear, 9634)
    elseif (self:GetUnitCreatureId(uId) == 23863 and phaseCounter == 2 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.62) then
        phaseCounter = phaseCounter + 1
        warnNextPhaseSoon:Show(L.Hawk, 40120)
    elseif (self:GetUnitCreatureId(uId) == 23863 and phaseCounter == 3 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.42) then
        phaseCounter = phaseCounter + 1
        warnNextPhaseSoon:Show(L.Lynx, 768)
    elseif (self:GetUnitCreatureId(uId) == 23863 and phaseCounter == 4 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.22) then
        phaseCounter = phaseCounter + 1
        warnNextPhaseSoon:Show(L.Dragon, 61846)
    end
end

function mod:PillarTimer()
    timerFlamePillarCD:Start()
    self:UnscheduleMethod("PillarTimer")
    self:ScheduleMethod(10, "PillarTimer")
end

function mod:OnSync(msg, target)
    if msg == "Pillar" then
        warnFlamePillar:Show(target)
        if self.Options.SetIconOnFlamePillar then
            self:SetIcon(target, 8, 4)
        end
        if target == UnitName("player") then
            self:PlaySound("fear2")
            specWarnFlamePillar:Show()
            yellFlamePillar:Yell()
        elseif target then
            local uId = DBM:GetRaidUnitId(target)
            local x, y = GetPlayerMapPosition(uId)
            if x == 0 and y == 0 then
                SetMapToCurrentZone()
                x, y = GetPlayerMapPosition(uId)
            end
            if uId and (DBM.RangeCheck:GetDistance(uId, GetPlayerMapPosition("player")) < 4) then
                specWarnFlamePillarNear:Show()
                if self.Options.PillarArrow then
                    DBM.Arrow:ShowRunAway(x, y, 6, 4)
                end
            end
        end
        self:PillarTimer()
    end
end

function mod:OnCombatEnd()
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
end
