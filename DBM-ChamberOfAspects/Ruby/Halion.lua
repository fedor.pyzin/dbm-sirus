local mod = DBM:NewMod("Halion", "DBM-ChamberOfAspects", 2)
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4870 $"):sub(12, -3))
mod:SetCreatureID(39863)--40142 (twilight form)
mod:SetMinSyncRevision(4358)
mod:SetUsedIcons(7, 8)

mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell", L.YellKill)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "SPELL_DAMAGE",
    "CHAT_MSG_MONSTER_YELL",
    "CHAT_MSG_RAID_BOSS_EMOTE",
    "UNIT_HEALTH"
)

local warnPhase2Soon                = mod:NewAnnounce("WarnPhase2Soon", 2)
local warnPhase3Soon                = mod:NewAnnounce("WarnPhase3Soon", 2)
local warnPhase2                    = mod:NewPhaseAnnounce(2)
local warnPhase3                    = mod:NewPhaseAnnounce(3)
local warningShadowConsumption      = mod:NewTargetAnnounce(74792, 4)
local warningFieryConsumption       = mod:NewTargetAnnounce(74562, 4)
local warningMeteor                 = mod:NewSpellAnnounce(74648, 3)
local warningShadowBreath           = mod:NewSpellAnnounce(75954, 2, nil, mod:IsTank() or mod:IsHealer())
local warningFieryBreath            = mod:NewSpellAnnounce(74526, 2, nil, mod:IsTank() or mod:IsHealer())
local warningTwilightCutter         = mod:NewAnnounce("TwilightCutterCast", 4, 77844)

local specWarnShadowConsumption     = mod:NewSpecialWarningRun(74792)
local specWarnFieryConsumption      = mod:NewSpecialWarningRun(74562)
local specWarnMeteorStrike          = mod:NewSpecialWarningMove(75952)
local specWarnTwilightCutter        = mod:NewSpecialWarningSpell(77844)

local yellShadow                    = mod:NewYell(74792)
local yellFiery                     = mod:NewYell(74562)

local timerShadowConsumptionCD      = mod:NewNextTimer(20, 74792)
local timerFieryConsumptionCD       = mod:NewNextTimer(20, 74562)
local timerMeteorCD                 = mod:NewNextTimer(40, 74648)
local timerTwilightCutterCD         = mod:NewNextTimer(30, 77844)
local timerShadowBreathCD           = mod:NewCDTimer(19, 75954)
local timerFieryBreathCD            = mod:NewCDTimer(19, 74526)

local berserkTimer                  = mod:NewBerserkTimer(480)

mod:AddBoolOption("AnnounceAlternatePhase", true, "announce")
mod:AddSetIconOption("SetIconOnFiery", 74562, {8}, true)
mod:AddSetIconOption("SetIconOnShadow", 74792, {7}, true)

local warned_preP2 = false
local warned_preP3 = false
local lastshroud = 0
local shroudtrig = true
local phases = {}

function mod:LocationChecker()
    if GetTime() - lastshroud < 6 then
        DBM.BossHealth:RemoveBoss(39863)
    else
        DBM.BossHealth:RemoveBoss(40142)
    end
end

local function updateHealthFrame(phase)
    if phases[phase] then
        return
    end
    phases[phase] = true
    if phase == 1 then
        DBM.BossHealth:Clear()
        DBM.BossHealth:AddBoss(39863, L.NormalHalion)
    elseif phase == 2 then
        DBM.BossHealth:Clear()
        DBM.BossHealth:AddBoss(40142, L.TwilightHalion)
    elseif phase == 3 then
        DBM.BossHealth:AddBoss(39863, L.NormalHalion)
        mod:ScheduleMethod(20, "LocationChecker")
    end
end

function mod:OnCombatStart(delay)
    table.wipe(phases)
    warned_preP2 = false
    warned_preP3 = false
    phase2Started = 0
    lastshroud = 0
    shroudtrig = true
    berserkTimer:Start(-delay)
    timerMeteorCD:Start(20-delay)
    timerFieryConsumptionCD:Start(15-delay)
    timerFieryBreathCD:Start(10-delay)
    updateHealthFrame(1)
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(74806, 75954, 75955, 75956) then
        warningShadowBreath:Show()
        timerShadowBreathCD:Start()
    elseif args:IsSpellID(74525, 74526, 74527, 74528) then
        warningFieryBreath:Show()
        timerFieryBreathCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(74792) then
        if not self.Options.AnnounceAlternatePhase then
            warningShadowConsumption:Show(args.destName)
            timerShadowConsumptionCD:Start()
        end
        if mod:LatencyCheck() then
            self:SendSync("ShadowTarget", args.destName)
        end
        if args:IsPlayer() then
            specWarnShadowConsumption:Show()
            yellShadow:Yell()
            self:PlaySound("fear2", "bochok", "t_zaminirovali", "gazlaytiat")
        end
        if self.Options.SetIconOnShadow then
            self:SetIcon(args.destName, 7)
        end
    elseif args:IsSpellID(74562) then
        if not self.Options.AnnounceAlternatePhase then
            warningFieryConsumption:Show(args.destName)
            timerFieryConsumptionCD:Start()
        end
        if mod:LatencyCheck() then
            self:SendSync("FieryTarget", args.destName)
        end
        if args:IsPlayer() then
            specWarnFieryConsumption:Show()
            yellFiery:Yell()
            self:PlaySound("fear2", "bochok", "t_zaminirovali", "gazlaytiat")
        end
        if self.Options.SetIconOnFiery then
            self:SetIcon(args.destName, 8)
        end
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(74792) then
        if self.Options.SetIconOnShadow then
            self:SetIcon(args.destName, 0)
        end
    elseif args:IsSpellID(74562) then
        if self.Options.SetIconOnFiery then
            self:SetIcon(args.destName, 0)
        end
    end
end

local cutterKilled = 0
function mod:ResetCutterKillCounter()
    cutterKilled = 0
end

function mod:SPELL_DAMAGE(args)
    if (args:IsSpellID(75952, 75951, 75950, 75949) or args:IsSpellID(75948, 75947)) and args:IsPlayer() and self:AntiSpam(2, "flame") then
        specWarnMeteorStrike:Show()
    elseif args:IsSpellID(75483, 75484, 75485, 75486) and args:IsPlayer() then
        if shroudtrig then
            shroudtrig = false
            self:PlaySound("w_rice")
        end
        lastshroud = GetTime()
    elseif args:IsSpellID(77844, 77845, 77846, 77849) and args:IsDestTypePlayer() and args.overkill > 0 then
        cutterKilled = cutterKilled + 1
        if cutterKilled >= 5 and self:AntiSpam(10, "masskill") then
            self:PlaySound("s_sdelal")
        end
        self:UnscheduleMethod("ResetCutterKillCounter")
        self:ScheduleMethod(1, "ResetCutterKillCounter")
    end
end

function mod:UNIT_HEALTH(uId)
    if not warned_preP2 and self:GetUnitCreatureId(uId) == 39863 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.79 then
        warned_preP2 = true
        warnPhase2Soon:Show()    
    elseif not warned_preP3 and self:GetUnitCreatureId(uId) == 40142 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.54 then
        warned_preP3 = true
        warnPhase3Soon:Show()    
    end
end

function mod:Meteor()
    warningMeteor:Show()
    timerMeteorCD:Start()
    self:UnscheduleMethod("Meteor")
    self:ScheduleMethod(40.1, "Meteor")
end

function mod:Cutter()
    warningTwilightCutter:Schedule(25)
    specWarnTwilightCutter:Show()
    timerTwilightCutterCD:Start(30)
    self:UnscheduleMethod("Cutter")
    self:ScheduleMethod(30.1, "Cutter")
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.Phase2 then
        updateHealthFrame(2)
        timerFieryBreathCD:Cancel()
        timerMeteorCD:Cancel()
        timerFieryConsumptionCD:Cancel()
        warnPhase2:Show()
        timerShadowBreathCD:Start(25)
        timerShadowConsumptionCD:Start(20)
        timerTwilightCutterCD:Start(40)
        warningTwilightCutter:Schedule(35.2)
    elseif msg == L.Phase3 then
        self:SendSync("Phase3")
    elseif msg == L.YellMeteor then
        if not self.Options.AnnounceAlternatePhase then
            self:Meteor()
        end
        if mod:LatencyCheck() then
            self:SendSync("Meteor")
        end
    elseif msg == L.YellCutter then
        if not self.Options.AnnounceAlternatePhase then
            self:Cutter()
        end
    elseif msg == L.YellKill then
        self:PlaySound("viderzhali")
    end
end

function mod:CHAT_MSG_RAID_BOSS_EMOTE(msg)
    if msg == L.EmoteCutter then
        warningTwilightCutter:Cancel()
        warningTwilightCutter:Show()
        self:ScheduleMethod(5.1, "Cutter")
        if mod:LatencyCheck() then
            self:SendSync("TwilightCutter")
        end
    end
end

function mod:OnSync(msg, target)
    if msg == "TwilightCutter" then
        if self.Options.AnnounceAlternatePhase then
            self:ScheduleMethod(5, "Cutter")
        end
    elseif msg == "Meteor" then
        if self.Options.AnnounceAlternatePhase then
            self:Meteor()
        end
    elseif msg == "ShadowTarget" then
        if self.Options.AnnounceAlternatePhase then
            warningShadowConsumption:Show(target)
            timerShadowConsumptionCD:Start()
        end
    elseif msg == "FieryTarget" then
        if self.Options.AnnounceAlternatePhase then
            warningFieryConsumption:Show(target)
            timerFieryConsumptionCD:Start()
        end
    elseif msg == "Phase3" then
        updateHealthFrame(3)
        warnPhase3:Show()
        timerMeteorCD:Start(30)
        timerFieryConsumptionCD:Start()
    end
end
