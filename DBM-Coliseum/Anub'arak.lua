﻿local mod = DBM:NewMod("Anub'arak_Coliseum", "DBM-Coliseum")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4520 $"):sub(12, -3))
mod:SetCreatureID(34564)  

mod:RegisterCombat("yell", L.YellPull)

mod:RegisterEvents(
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REFRESH",     
    "SPELL_AURA_REMOVED",
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "CHAT_MSG_RAID_BOSS_EMOTE"
)

mod:SetUsedIcons(3, 4, 5, 6, 7, 8)


mod:AddBoolOption("RemoveHealthBuffsInP3", false)

-- Adds
local warnAdds              = mod:NewAnnounce("warnAdds", 3, 45419)
local timerAdds             = mod:NewTimer(45, "timerAdds", 45419)
local Burrowed              = false 

-- Pursue
local warnPursue            = mod:NewTargetAnnounce(67574, 4)
local specWarnPursue        = mod:NewSpecialWarning("SpecWarnPursue")
local warnHoP               = mod:NewTargetAnnounce(10278, 2, nil, false)
local timerHoP              = mod:NewBuffActiveTimer(10, 10278, nil, false)
mod:AddSetIconOption("PursueIcon", 67574, {8}, true)

-- Emerge
local warnEmerge            = mod:NewAnnounce("WarnEmerge", 3, "Interface\\AddOns\\DBM-Core\\textures\\CryptFiendUnBurrow.blp")
local warnEmergeSoon        = mod:NewAnnounce("WarnEmergeSoon", 1, "Interface\\AddOns\\DBM-Core\\textures\\CryptFiendUnBurrow.blp")
local timerEmerge           = mod:NewTimer(60, "TimerEmerge", "Interface\\AddOns\\DBM-Core\\textures\\CryptFiendUnBurrow.blp")

-- Submerge
local warnSubmerge          = mod:NewAnnounce("WarnSubmerge", 3, "Interface\\AddOns\\DBM-Core\\textures\\CryptFiendBurrow.blp")
local warnSubmergeSoon      = mod:NewAnnounce("WarnSubmergeSoon", 1, "Interface\\AddOns\\DBM-Core\\textures\\CryptFiendBurrow.blp")
local specWarnSubmergeSoon  = mod:NewSpecialWarning("specWarnSubmergeSoon", mod:IsTank())
local timerSubmerge         = mod:NewTimer(80, "TimerSubmerge", "Interface\\AddOns\\DBM-Core\\textures\\CryptFiendBurrow.blp")

-- Phases
local warnPhase3            = mod:NewPhaseAnnounce(3)
local enrageTimer           = mod:NewBerserkTimer(600)

-- Penetrating Cold
local warnPColdTargets      = mod:NewTargetAnnounce(68510, 3)
local specWarnPCold         = mod:NewSpecialWarningYou(68510, false)
local timerPCold            = mod:NewBuffActiveTimer(15, 68509)
mod:AddSetIconOption("SetIconsOnPCold", 66013, {7, 6, 5, 4, 3}, true)

-- Freezing Slash
local warnFreezingSlash     = mod:NewTargetAnnounce(66012, 2, nil, mod:IsHealer() or mod:IsTank())
local timerFreezingSlash    = mod:NewCDTimer(15, 66012, nil, mod:IsHealer() or mod:IsTank())

mod:AddAnnounceLine(DBM_HEROIC_MODE)
mod:AddTimerLine(DBM_HEROIC_MODE)
-- Shadow Strike
local timerShadowStrike     = mod:NewNextTimer(30, 66134)
local preWarnShadowStrike   = mod:NewSoonAnnounce(66134, 3)
local warnShadowStrike      = mod:NewSpellAnnounce(66134, 4)
local specWarnShadowStrike  = mod:NewSpecialWarning("SpecWarnShadowStrike", mod:IsTank())

function mod:OnCombatStart(delay)
    Burrowed = false 
    timerAdds:Start(10-delay) 
    warnAdds:Schedule(10-delay) 
    self:ScheduleMethod(10-delay, "Adds")
    warnSubmergeSoon:Schedule(70-delay)
    specWarnSubmergeSoon:Schedule(70-delay)
    timerSubmerge:Start(-delay)
    enrageTimer:Start(-delay)
    timerFreezingSlash:Start(-delay)
    if self:IsDifficulty("heroic10", "heroic25") then
        timerShadowStrike:Start()
        preWarnShadowStrike:Schedule(25)
        self:ScheduleMethod(30, "ShadowStrike")
    end
end

function mod:Adds() 
    if self:IsInCombat() and not Burrowed then 
        timerAdds:Start() 
        warnAdds:Schedule(45)
        self:UnscheduleMethod("Adds")
        self:ScheduleMethod(45, "Adds")
    end 
end

function mod:ShadowStrike()
    if self:IsInCombat() then
        timerShadowStrike:Start()
        preWarnShadowStrike:Cancel()
        preWarnShadowStrike:Schedule(25)
        self:UnscheduleMethod("ShadowStrike")
        self:ScheduleMethod(30, "ShadowStrike")
    end
end

local PColdTargets = {}
local PColdIcon = 7

function mod:HandlePCold()
    warnPColdTargets:Show(table.concat(PColdTargets, "<, >"))
    if self.Options.SetIconsOnPCold then
        table.sort(PColdTargets, function(v1,v2) return DBM:GetRaidSubgroup(v1) < DBM:GetRaidSubgroup(v2) end)
        PColdIcon = 7
        for i, v in ipairs(PColdTargets) do
            self:SetIcon(v, PColdIcon)
            PColdIcon = PColdIcon - 1
        end 
    end
    table.wipe(PColdTargets)
end


function mod:SPELL_AURA_REFRESH(args)
    if args:IsSpellID(66013, 67700, 68509, 68510) then  -- Penetrating Cold
        if args:IsPlayer() then
            specWarnPCold:Show()
        end
        PColdTargets[#PColdTargets + 1] = args.destName
        self:UnscheduleMethod("HandlePCold")
        self:ScheduleMethod(0.02, "HandlePCold")
        timerPCold:Show() 
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(67574) then            -- Pursue
        if args:IsPlayer() then
            specWarnPursue:Show()
            self:PlaySound("fear2", "run")
        end
        if self.Options.PursueIcon then
            self:SetIcon(args.destName, 8, 15)
        end
        warnPursue:Show(args.destName)
    elseif args:IsSpellID(66013, 67700, 68509, 68510) then        -- Penetrating Cold
        if args:IsPlayer() then
            specWarnPCold:Show()
        end
        PColdTargets[#PColdTargets + 1] = args.destName
        self:UnscheduleMethod("HandlePCold")
        self:ScheduleMethod(0.02, "HandlePCold")
        timerPCold:Show() 
    elseif args:IsSpellID(10278) and self:IsInCombat() then        -- Hand of Protection
        warnHoP:Show(args.destName)
        timerHoP:Start(args.destName)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(66013, 67700, 68509, 68510) then            -- Penetrating Cold
        mod:SetIcon(args.destName, 0)
    elseif args:IsSpellID(10278) and self:IsInCombat() then        -- Hand of Protection
        timerHoP:Cancel(args.destName)
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(66118, 67630, 68646, 68647) then            -- Swarm (start p3)
        warnPhase3:Show()
        warnEmergeSoon:Cancel()
        warnSubmergeSoon:Cancel()
        specWarnSubmergeSoon:Cancel()
        timerEmerge:Cancel()
        timerSubmerge:Cancel()
        if self.Options.RemoveHealthBuffsInP3 then
            self:ScheduleMethod(0.1, "RemoveBuffs")
        end
        if mod:IsDifficulty("normal10", "normal25") then
            timerAdds:Cancel()
            warnAdds:Cancel()
            self:UnscheduleMethod("Adds")
        end
    elseif args:IsSpellID(66134) then                            -- Shadow Strike
        self:ShadowStrike()
        specWarnShadowStrike:Show()
        warnShadowStrike:Show()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(66012) then                            -- Freezing Slash
        warnFreezingSlash:Show(args.destName)
        timerFreezingSlash:Start()
    end
end

function mod:CHAT_MSG_RAID_BOSS_EMOTE(msg)
    if msg and msg:find(L.Burrow) then
        Burrowed = true
        timerAdds:Cancel()
        warnAdds:Cancel()
        warnSubmerge:Show()
        warnEmergeSoon:Schedule(50)
        timerEmerge:Start()
        timerFreezingSlash:Cancel()
    elseif msg and msg:find(L.Emerge) then
        Burrowed = false
        timerAdds:Start(10)
        warnAdds:Schedule(10)
        self:ScheduleMethod(10, "Adds")
        warnEmerge:Show()
        warnSubmergeSoon:Schedule(70)
        specWarnSubmergeSoon:Schedule(70)
        timerFreezingSlash:Start()
        timerSubmerge:Start()
        if self:IsDifficulty("heroic10", "heroic25") then
            self:ShadowStrike()
        end
    end
end

function mod:OnCombatEnd()
    self:UnscheduleMethod("Adds")
    self:UnscheduleMethod("ShadowStrike")
end

function mod:RemoveBuffs()
    CancelUnitBuff("player", (GetSpellInfo(47440)))        -- Commanding Shout
    CancelUnitBuff("player", (GetSpellInfo(48161)))        -- Power Word: Fortitude
    CancelUnitBuff("player", (GetSpellInfo(48162)))        -- Prayer of Fortitude
    CancelUnitBuff("player", (GetSpellInfo(69377)))        -- Runescroll of Fortitude
end

