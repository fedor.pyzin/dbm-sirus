local mod = DBM:NewMod("IronCouncil", "DBM-Ulduar")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4154 $"):sub(12, -3))
mod:SetCreatureID(32927)
mod:SetUsedIcons(1, 2, 3, 4, 5, 6, 7, 8)

mod:RegisterCombat("combat", 32867, 32927, 32857)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_CAST_SUCCESS",
    "CHAT_MSG_MONSTER_YELL"
)

mod:AddBoolOption("HealthFrame", true)

mod:SetBossHealthInfo(
    32867, L.Steelbreaker,
    32927, L.RunemasterMolgeim,
    32857, L.StormcallerBrundir
)

local warnSupercharge           = mod:NewSpellAnnounce(61920, 3)

-- Stormcaller Brundir
-- High Voltage ... 63498
local warnChainlight            = mod:NewSpellAnnounce(64215, 1)
local timerOverload             = mod:NewCastTimer(6, 63481)
local timerOverloadCD           = mod:NewCDTimer(60, 63481)
local timerLightningWhirl       = mod:NewCastTimer(5, 63483)
local specwarnLightningTendrils = mod:NewSpecialWarningRun(63486)
local timerLightningTendrils    = mod:NewBuffActiveTimer(27, 63486)
local timerLightningTendrilsCD  = mod:NewCDTimer(90, 63486)
local specwarnOverload          = mod:NewSpecialWarningRun(63481) 
mod:AddBoolOption("AlwaysWarnOnOverload", false, "announce")

-- Steelbreaker
-- High Voltage ... don't know what to show here - 63498
local warnFusionPunch           = mod:NewSpellAnnounce(61903, 4)
local timerFusionPunchCast      = mod:NewCastTimer(3, 61903)
local timerFusionPunchActive    = mod:NewTargetTimer(4, 61903)
local warnOverwhelmingPower     = mod:NewTargetAnnounce(61888, 2)
local timerOverwhelmingPower    = mod:NewTargetTimer(25, 61888)
local warnStaticDisruption      = mod:NewTargetAnnounce(61912, 3) 
mod:AddSetIconOption("SetIconOnOverwhelmingPower", 61888, {8}, true)
mod:AddSetIconOption("SetIconOnStaticDisruption", 61912, nil, true)

-- Runemaster Molgeim
-- Lightning Blast ... don't know, maybe 63491
local timerShieldofRunes        = mod:NewBuffActiveTimer(15, 63967)
local warnRuneofPower           = mod:NewTargetAnnounce(64320, 2)
local warnRuneofDeath           = mod:NewSpellAnnounce(63490, 2)
local warnShieldofRunes         = mod:NewSpellAnnounce(63489, 2)
local warnRuneofSummoning       = mod:NewSpellAnnounce(62273, 3)
local specwarnRuneofDeath       = mod:NewSpecialWarningMove(63490)
local timerRuneofPower          = mod:NewCDTimer(35, 61974)
local timerRuneofDeath          = mod:NewCDTimer(30, 63490)

local enrageTimer               = mod:NewBerserkTimer(900)

local disruptTargets = {}
local disruptIcon = 7
local runemasterAlive = true
local brundirAlive = true
local steelbreakerAlive = true

function mod:Overload()
    timerOverloadCD:Start()
    self:ScheduleMethod(60, "Overload")
end

function mod:OnCombatStart(delay)
    enrageTimer:Start(-delay)
    timerRuneofPower:Start(21)
    self:UnscheduleMethod("Overload")
    self:ScheduleMethod(72, "Overload")
    timerOverloadCD:Start(72)
    table.wipe(disruptTargets)
    disruptIcon = 7
    runemasterAlive = true
    brundirAlive = true
    steelbreakerAlive = true
end

function mod:RuneTarget()
    local targetname = self:GetBossTarget(32927)
    if targetname then 
        warnRuneofPower:Show(targetname)
    end    
end

local function warnStaticDisruptionTargets()
    warnStaticDisruption:Show(table.concat(disruptTargets, "<, >"))
    table.wipe(disruptTargets)
    disruptIcon = 7
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(61920) then -- Supercharge - Unleashes one last burst of energy as the caster dies, increasing all allies damage by 25% and granting them an additional ability.    
        warnSupercharge:Show()
    elseif args:IsSpellID(63479, 61879) then    -- Chain light
        warnChainlight:Show()
    elseif args:IsSpellID(61903, 63493) then    -- Fusion Punch
        warnFusionPunch:Show()
        timerFusionPunchCast:Start()
    elseif args:IsSpellID(62274, 63489) then        -- Shield of Runes
        warnShieldofRunes:Show()
    elseif args:IsSpellID(62273) then                -- Rune of Summoning
        warnRuneofSummoning:Show()
    elseif args:IsSpellID(64321, 61973, 61974) then    -- Rune of Power
        self:ScheduleMethod(0.1, "RuneTarget")
        timerRuneofPower:Start()
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.RuneOfDeathYell then        -- Rune of Death
        warnRuneofDeath:Show()
        timerRuneofDeath:Start()
    elseif msg == L.YellSteelbreakerDied or msg == L.YellSteelbreakerDied2 then
        steelbreakerAlive = false
        if runemasterAlive and brundirAlive then
            timerRuneofPower:Start(25)
            timerRuneofDeath:Start()
            timerShieldofRunes:Start(27)
        elseif not runemasterAlive and brundirAlive then
            timerLightningTendrilsCD:Start(53)
        end
    elseif msg == L.YellStormcallerBrundirDied or msg == L.YellStormcallerBrundirDied2 then
        brundirAlive = false
        timerOverloadCD:Cancel()
        self:UnscheduleMethod("Overload")
        if runemasterAlive and steelbreakerAlive then
            timerRuneofPower:Start(25)
            timerRuneofDeath:Start()
            timerShieldofRunes:Start(27)
        end
    elseif msg == L.YellRunemasterMolgeimDied or msg == L.YellRunemasterMolgeimDied2 then
        runemasterAlive = false
        timerRuneofDeath:Stop()
        timerRuneofPower:Stop()
        if not steelbreakerAlive and brundirAlive then
            timerLightningTendrilsCD:Start(53)
        end
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(61869, 63481) then    -- Overload
        timerOverload:Start()
        timerOverloadCD:Start()
        if self.Options.AlwaysWarnOnOverload or UnitName("target") == L.StormcallerBrundir then
            specwarnOverload:Show()
        end
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(61903, 63493) then        -- Fusion Punch
        timerFusionPunchActive:Start(args.destName)
    elseif args:IsSpellID(63483, 61915) then    -- LightningWhirl
        timerLightningWhirl:Start()
    elseif args:IsSpellID(62269, 63490) then    -- Rune of Death - move away from it
        if args:IsPlayer() then
            specwarnRuneofDeath:Show()
        end
    elseif args:IsSpellID(62277, 63967) and not args:IsDestTypePlayer() then        -- Shield of Runes
        timerShieldofRunes:Start()        
    elseif args:IsSpellID(64637, 61888) then    -- Overwhelming Power
        warnOverwhelmingPower:Show(args.destName)
        if mod:IsDifficulty("heroic10") then
            timerOverwhelmingPower:Start(60, args.destName)
        else
            timerOverwhelmingPower:Start(35, args.destName)
        end
        if self.Options.SetIconOnOverwhelmingPower then
            if mod:IsDifficulty("heroic10") then
                self:SetIcon(args.destName, 8, 60) -- skull for 60 seconds (until meltdown)
            else
                self:SetIcon(args.destName, 8, 35) -- skull for 35 seconds (until meltdown)
            end
        end
        if args:IsPlayer() then
            DBM.RangeCheck:Show(20)
        end
    elseif args:IsSpellID(63486, 61887) then    -- Lightning Tendrils
        timerLightningTendrils:Start()
        timerLightningTendrilsCD:Start()
        specwarnLightningTendrils:Show()
    elseif args:IsSpellID(61912, 63494) then    -- Static Disruption (Hard Mode)
        disruptTargets[#disruptTargets + 1] = args.destName
        if self.Options.SetIconOnStaticDisruption then 
            self:SetIcon(args.destName, disruptIcon, 20)
            disruptIcon = disruptIcon - 1
        end
        self:Unschedule(warnStaticDisruptionTargets)
        self:Schedule(0.3, warnStaticDisruptionTargets)
    end
end

function mod:OnCombatEnd()
    DBM.BossHealth:Hide()
    DBM.RangeCheck:Hide()
    self:UnscheduleMethod("Overload")
end