local mod = DBM:NewMod("Mimiron", "DBM-Ulduar")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4338 $"):sub(12, -3))
mod:SetCreatureID(33432)

mod:RegisterCombat("yell", L.YellPull)
mod:RegisterCombat("yell", L.YellHardPull)
mod:RegisterKill("yell", L.YellKilled)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "CHAT_MSG_MONSTER_YELL",
    "CHAT_MSG_LOOT",
    "UNIT_SPELLCAST_SUCCEEDED"
)

local blastWarn                 = mod:NewTargetAnnounce(64529, 4)
local lootannounce              = mod:NewAnnounce("MagneticCore", 1)
local warnBombSpawn             = mod:NewAnnounce("WarnBombSpawn", 3)
local warnFrostBomb             = mod:NewSpellAnnounce(64623, 3)
local warnShockBlast            = mod:NewSpecialWarningRun(63631, mod:IsMelee())
local warnDarkGlare             = mod:NewSpecialWarningSpell(63293)
local specWarnPlasma            = mod:NewSpecialWarningSpell(64529, mod:IsTank() or mod:IsHealer())
local warnFlamesSoon            = mod:NewSoonAnnounce(64566, 4) 

local timerEnrage               = mod:NewBerserkTimer(900)

local timerP1toP2               = mod:NewTimer(40.5, "TimeToPhase2")
local timerP2toP3               = mod:NewTimer(20, "TimeToPhase3")
local timerP3toP4               = mod:NewTimer(27, "TimeToPhase4")

local timerShockBlast           = mod:NewCastTimer(4, 63631)
local timerPlasmaBlast          = mod:NewCastTimer(9, 64529)
local timerDarkGlareCast        = mod:NewCastTimer(10, 63274)
local timerBombExplosion        = mod:NewCastTimer(15, 65333)
local timerSpinUp               = mod:NewCastTimer(4, 63414)

local timerNextShockblast       = mod:NewNextTimer(30, 63631)
local timerNextPlasmaBlast      = mod:NewNextTimer(35, 64529)
local timerNextDarkGlare        = mod:NewNextTimer(41, 63274)
local timerNextFrostBomb        = mod:NewNextTimer(30, 64623)

local timerProximityMinesCD        = mod:NewCDTimer(35, 63027)
local timerFlameSuppressantP1CD    = mod:NewCDTimer(60, 64570)
local timerFlameSuppressantP2CD    = mod:NewCDTimer(12, 65192)
local timerFlamesCD                = mod:NewCDTimer(28, 64566)
local timerBombBotCD               = mod:NewCDTimer(20, 63811)

mod:AddBoolOption("HealthFramePhase4", true)
mod:AddBoolOption("AutoChangeLootToFFA", true)
mod:AddBoolOption("RangeFrame")

local hardmode = false
local p4 = false
local lootmethod, masterlooterRaidID

function mod:Flames()
    timerFlamesCD:Start()
    warnFlamesSoon:Schedule(23)
    self:ScheduleMethod(28, "Flames")
end

function mod:BombBot()
    timerBombBotCD:Start()
    self:ScheduleMethod(20, "BombBot")
end

function mod:Mines()
    timerProximityMinesCD:Start(p4 and 49 or 35)
    self:ScheduleMethod(p4 and 49 or 35, "Mines")
end

function mod:OnCombatStart(delay)
    hardmode = false
    p4 = false
    
    timerEnrage:Start(-delay)
    timerNextPlasmaBlast:Start(27-delay)
    timerNextShockblast:Start(27-delay)
    
    timerProximityMinesCD:Start(18-delay)
    self:ScheduleMethod(18, "Mines")
    
    if DBM:GetRaidRank() == 2 then
        lootmethod, _, masterlooterRaidID = GetLootMethod()
    end
    if self.Options.RangeFrame then
        DBM.RangeCheck:Show(6)
    end
    if self.Options.HealthFrame then
        DBM.BossHealth:Clear()
        DBM.BossHealth:AddBoss(33432, L.MobPhase1)
    end
end

function mod:UNIT_SPELLCAST_SUCCEEDED(unit, spellName, ...)
    if UnitName(unit) == L.MobPhase3 and spellName == L.BombBot then
        warnBombSpawn:Show()
        timerBombBotCD:Start()
        self:UnscheduleMethod("BombBot")
        self:ScheduleMethod(20, "BombBot")
    end
end

function mod:CHAT_MSG_LOOT(msg)
    local player, itemID = msg:match(L.LootMsg)
    if player and itemID and tonumber(itemID) == 46029 then
        lootannounce:Show(player)
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(63631) then
        warnShockBlast:Show()
        timerShockBlast:Start()
        timerNextShockblast:Schedule(5)
    elseif args:IsSpellID(64529, 62997) then
        timerPlasmaBlast:Start()
        timerNextPlasmaBlast:Start()
        blastWarn:Show(args.destName)
        if args:IsPlayer() then
            specWarnPlasma:Show()
        end    
    elseif args:IsSpellID(64623) then
        warnFrostBomb:Show()
        timerBombExplosion:Start()
        timerNextFrostBomb:Start(p4 and 60 or 30)
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(63414) then
        warnDarkGlare:Show()
        timerSpinUp:Start()
        timerDarkGlareCast:Schedule(4)
        timerNextDarkGlare:Schedule(14)
    elseif args:IsSpellID(64570) then
        timerFlameSuppressantP1CD:Start()
    elseif args:IsSpellID(65192) then
        timerFlameSuppressantP2CD:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(64570) then
        timerFlameSuppressantP1CD:Start()
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellHardPull then
        hardmode = true
        p4 = false
        
        timerEnrage:Start(605)
        timerFlameSuppressantP1CD:Start(75)
        
        timerFlamesCD:Start(6)
        self:ScheduleMethod(6, "Flames")
    elseif msg == L.YellPhase2 then
        timerP1toP2:Start()
        
        timerNextShockblast:Cancel()
        timerNextPlasmaBlast:Cancel()
        timerProximityMinesCD:Cancel()
        timerFlameSuppressantP1CD:Cancel()
        
        timerFlameSuppressantP2CD:Schedule(40.5)
        self:UnscheduleMethod("Mines")
        
        if hardmode then timerNextFrostBomb:Start(45) end
        
        if self.Options.HealthFrame then
            DBM.BossHealth:Clear()
            DBM.BossHealth:AddBoss(33651, L.MobPhase2)
        end
        if self.Options.RangeFrame then
            DBM.RangeCheck:Hide()
        end
    elseif msg == L.YellPhase3 then
        if self.Options.AutoChangeLootToFFA and DBM:GetRaidRank() == 2 then
            SetLootMethod("freeforall")
        end
        timerP2toP3:Start()
        
        timerDarkGlareCast:Cancel()
        timerNextDarkGlare:Cancel()
        timerNextFrostBomb:Cancel()
        
        timerBombBotCD:Start(31)
        self:ScheduleMethod(31, "BombBot")
        
        if self.Options.HealthFrame then
            DBM.BossHealth:Clear()
            DBM.BossHealth:AddBoss(33670, L.MobPhase3)
        end
    elseif msg == L.YellPhase4 then
        p4 = true
        if self.Options.AutoChangeLootToFFA and DBM:GetRaidRank() == 2 then
            SetLootMethod(lootmethod, masterlooterRaidID and ("raid" .. masterlooterRaidID))
        end
        timerP3toP4:Start()
        
        timerBombBotCD:Cancel()
        self:UnscheduleMethod("BombBot")
        
        timerProximityMinesCD:Start(43)
        self:ScheduleMethod(43, "Mines")
        
        timerNextDarkGlare:Start(59)
        timerNextShockblast:Start(86)
        
        if self.Options.HealthFramePhase4 or self.Options.HealthFrame then
            DBM.BossHealth:Show(L.name)
            DBM.BossHealth:AddBoss(33670, L.MobPhase3)
            DBM.BossHealth:AddBoss(33651, L.MobPhase2)
            DBM.BossHealth:AddBoss(33432, L.MobPhase1)
        end
    end
end

function mod:OnCombatEnd()
    DBM.BossHealth:Hide()
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
    if self.Options.AutoChangeLootToFFA and DBM:GetRaidRank() == 2 then
        SetLootMethod(lootmethod, masterlooterRaidID and ("raid" .. masterlooterRaidID))
    end
    self:UnscheduleMethod("Flames")
    self:UnscheduleMethod("BombBot")
    self:UnscheduleMethod("Mines")
end