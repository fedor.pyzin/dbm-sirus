local mod = DBM:NewMod("Freya", "DBM-Ulduar")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4822 $"):sub(12, -3))

mod:SetCreatureID(32906)
mod:RegisterCombat("yell", L.YellPull)
mod:RegisterKill("yell", L.YellKill)
mod:SetUsedIcons(4, 5, 6, 7, 8)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "UNIT_DIED",
    "CHAT_MSG_MONSTER_YELL",
    "CHAT_MSG_RAID_BOSS_EMOTE"
)

-- Trash: 33430 Guardian Lasher (flower)
-- 33355 (nymph)
-- 33354 (tree)

--
-- Elder Stonebark (ground tremor / fist of stone)
-- Elder Brightleaf (unstable sunbeam)

local warnPhase2            = mod:NewPhaseAnnounce(2, 3)
local warnSimulKill         = mod:NewAnnounce("WarnSimulKill", 1)
local warnFury              = mod:NewTargetAnnounce(63571, 2)
local warnRoots             = mod:NewTargetAnnounce(62438, 2)
local warnEonarSoon         = mod:NewSoonAnnounce(62584, 3)

local specWarnFury          = mod:NewSpecialWarningYou(63571)
local specWarnEonar         = mod:NewSpecialWarningSpell(62584)
local specWarnTremor        = mod:NewSpecialWarningCast(62859)    -- Hard mode
local specWarnBeam          = mod:NewSpecialWarningMove(62865)    -- Hard mode

local enrage                = mod:NewBerserkTimer(600)
local timerAlliesOfNature   = mod:NewNextTimer(60, 62678)
local timerSimulKill        = mod:NewTimer(12, "TimerSimulKill")
local timerFury             = mod:NewTargetTimer(10, 63571)
local timerTremorCD         = mod:NewCDTimer(28, 62859)
local timerEonarCD          = mod:NewCDTimer(40, 62584)
local timerEonarHeal        = mod:NewTimer(14, "TimerEonarHeal", 24998)
local timerRootsCD          = mod:NewCDTimer(30, 63601)

mod:AddSetIconOption("SetIconOnFury", 63571, {8, 7}, true)
mod:AddSetIconOption("SetIconOnRoots", 63601, {6, 5, 4}, true)
mod:AddBoolOption("HealthFrame", true)

local adds            = {}
local rootedPlayers   = {}
local altIcon         = true
local killTime        = 0
local iconId          = 6

function mod:OnCombatStart(delay)
    altIcon = true
    iconId    = 6
    table.wipe(adds)
    table.wipe(rootedPlayers)
    enrage:Start()
    timerAlliesOfNature:Start(10)
    timerEonarCD:Start(25)
    warnEonarSoon:Schedule(20)
end

function mod:OnCombatEnd(wipe)
    warnEonarSoon:Cancel()
    DBM.BossHealth:Hide()
    if not wipe then
        if DBM.Bars:GetBar(L.TrashRespawnTimer) then
            DBM.Bars:CancelBar(L.TrashRespawnTimer) 
        end    
    end
end

local function showRootWarning()
    warnRoots:Show(table.concat(rootedPlayers, "< >"))
    table.wipe(rootedPlayers)
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(62437, 62859) then
        specWarnTremor:Show()
        timerTremorCD:Start()
    end
end 

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(63571, 62589) then
        if self.Options.SetIconOnFury then
            altIcon = not altIcon
            self:SetIcon(args.destName, altIcon and 7 or 8, 10)
        end
        warnFury:Show(args.destName)
        if args:IsPlayer() then
            specWarnFury:Show()
        end
        timerFury:Start(args.destName)
    elseif args:IsSpellID(63601) then
        timerRootsCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(62861, 62438) then
        if self.Options.SetIconOnRoots then
            iconId = iconId - 1
            self:SetIcon(args.destName, iconId, 15)
        end
        table.insert(rootedPlayers, args.destName)
        self:Unschedule(showRootWarning)
        if #rootedPlayers >= 3 then
            showRootWarning()
        else
            self:Schedule(0.5, showRootWarning)
        end

    elseif args:IsSpellID(62451, 62865) and args:IsPlayer() then
        specWarnBeam:Show()
    end 
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(62519) then
        warnPhase2:Show()
        timerAlliesOfNature:Cancel()
    elseif args:IsSpellID(62861, 62438) then
        if self.Options.SetIconOnRoots then
            self:RemoveIcon(args.destName)
            iconId = iconId + 1
        end
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.SpawnYell1 then
        timerAlliesOfNature:Start()
        if self.Options.HealthFrame then
            if not adds[33202] then DBM.BossHealth:AddBoss(33202, L.WaterSpirit) end -- ancient water spirit
            if not adds[32916] then DBM.BossHealth:AddBoss(32916, L.Snaplasher) end  -- snaplasher
            if not adds[32919] then DBM.BossHealth:AddBoss(32919, L.StormLasher) end -- storm lasher
        end
        adds[33202] = true
        adds[32916] = true
        adds[32919] = true
    elseif msg == L.SpawnYell2 then
        self:ScheduleMethod(2, "PlaySound", "moygrib")
        timerAlliesOfNature:Start()
    elseif msg == L.SpawnYell3 then
        timerAlliesOfNature:Start()
    end
end

function mod:CHAT_MSG_RAID_BOSS_EMOTE(msg)
    if msg == L.EmoteEonar then
        specWarnEonar:Show()
        timerEonarHeal:Start()
        timerEonarCD:Start()
        warnEonarSoon:Schedule(35)
    end
end

function mod:UNIT_DIED(args)
    local cid = self:GetCIDFromGUID(args.destGUID)
    if cid == 33202 or cid == 32916 or cid == 32919 then
        if self.Options.HealthFrame then
            DBM.BossHealth:RemoveBoss(cid)
        end
        if (GetTime() - killTime) > 20 then
            killTime = GetTime()
            timerSimulKill:Start()
            warnSimulKill:Show()
        end
        adds[cid] = nil
        local counter = 0
        for i, v in pairs(adds) do
            counter = counter + 1
        end
        if counter == 0 then
            timerSimulKill:Stop()
        end
    elseif cid == 33228 then
        timerEonarHeal:Stop()
    end
end