local L

--Magtheridon
L = DBM:GetModLocalization("Magtheridon")

L:SetGeneralLocalization{
    name                = "Магтеридон"
}

L:SetTimerLocalization{
    Pull                = "Активация босса",
    TimerQuakeCD        = DBM_CORE_AUTO_TIMER_TEXTS.cd:format(GetSpellInfo(30572) or "unknown")
}

L:SetWarningLocalization{
    WarnPhase2soon      = "Скоро фаза 2",
    WarnHandOfMagt      = "Печать Магтеридона",
    WarnQuake           = DBM_CORE_AUTO_ANNOUNCE_TEXTS.spell:format(GetSpellInfo(30572) or "unknown")
}

L:SetOptionLocalization{
    WarnPhase2soon      = "Анонсировать переход на вторую фазу",
    WarnHandOfMagt      = "Спец предупреждение для целей $spell:305166",
    Pull                = "Отсчет времени до активации босса",
    TimerQuakeCD        = DBM_CORE_AUTO_TIMER_OPTIONS.cd:format(30572, GetSpellInfo(30572) or "unknown"),
    WarnQuake           = DBM_CORE_AUTO_ANNOUNCE_OPTIONS.spell:format(30572, GetSpellInfo(30572) or "unknown")
}

L:SetMiscLocalization{
    YellPullAcolytes    = "Сдерживающая сила |3-1(%s) начинает ослабевать!",
    YellPull            = "Я... свободен!",
    Quake               = "Сотрясение",
    YellPhase2          = "Меня так просто не возьмешь! Пусть стены темницы содрогнутся... и падут!"
}