local L

--Maulgar
L = DBM:GetModLocalization("Maulgar")

L:SetGeneralLocalization{
    name                    = "Король Молгар"
}

L:SetTimerLocalization{
    TimerActive             = "%s активен"
}

L:SetWarningLocalization{
    WarnMight               = "%s активен",
    Move                    = "%s отойдите!",
    WarnDecay               = "%s на |3-5(>%s<) (%s)",
    WarnDecayH              = "%s на |3-5(>%s<) (%s)",
}

L:SetOptionLocalization{
    WarnMight               = "Обявлять активацию минибоссов",
    Move                    = "Обявлять опасные способности для милизоны",
    WarnDecay               = "Объявлять количестве стаков на цели $spell:33129",
    WarnDecayH              = "Объявлять количестве стаков на цели $spell:302287",
    AnnounceToChat          = "Анонсировать об активации минибоссов в чат",
    AnnounceActive          = " активен"
}

L:SetMiscLocalization{
    YellPull                = "Единственная настоящая сила в Запределье – это Гронн!",
    YellKill                = "Груул... сокрушит вас"
}

--Gruul
L = DBM:GetModLocalization("Gruul")

L:SetGeneralLocalization{
    name                    = "Груул Драконобой"
}

L:SetTimerLocalization{
    TimerRockCall           = "Хлопок!",
    TimerFurnaceActive      = "Печь активна",
    TimerFurnaceInactive    = "Печь не активна",
    TimerBurnedFlesh        = "Обожженная плоть (х2 урон)"
}

L:SetWarningLocalization{
    SpecWarnBurnedFlesh     = "Двойной урон!"
}

L:SetOptionLocalization{
    TimerRockCall           = "Отсчет времени до хлопка $spell:305188",
    TimerFurnaceActive      = "Отсчет времени пока печь активна",
    TimerFurnaceInactive    = "Отсчет времени пока печь не активна",
    TimerBurnedFlesh        = "Отсчет времени пока длится $spell:305204",
    SpecWarnBurnedFlesh     = "Спец-предупреждение о $spell:305204 на боссе"
}

L:SetMiscLocalization{
}
