local mod = DBM:NewMod("Attumen", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4830 $"):sub(12, -3))
mod:SetCreatureID(15550, 16151, 16152, 34971, 34972, 34973)
mod:RegisterCombat("combat", 16152)
--mod:RegisterKill("yell", L.KillAttumen)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_SUMMON",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "CHAT_MSG_MONSTER_EMOTE",
    "UNIT_HEALTH"
)

local warnPhase2            = mod:NewPhaseAnnounce(2)
local warnPhase3            = mod:NewPhaseAnnounce(3)
local warnAttumenSoon       = mod:NewSoonAnnounce(29799, 4)
local warnCurseSoon         = mod:NewSoonAnnounce(29833, 2)
local warnCurse             = mod:NewSpellAnnounce(29833, 3)
local warnAttumen           = mod:NewAnnounce("WarnAttumen", 4)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnInv               = mod:NewSpellAnnounce(305251, 4)
local warnGhost             = mod:NewTargetAnnounce(8326, 2)
local warnPhase2Soon        = mod:NewAnnounce("WarnPhase2Soon", 1)

local specWarnGhost         = mod:NewSpecialWarningYou(8326)

local yellGhost             = mod:NewYell(8326)

local timerCurseCD          = mod:NewNextTimer(35, 29833)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerInvCD            = mod:NewCDTimer(21, 305251)
local timerChargeCD         = mod:NewCDTimer(11, 305258)
local timerChargeP2CD       = mod:NewCDTimer(15, 305263)
local timerSufferingCD      = mod:NewCDTimer(21, 305259)
local timerTrampCD          = mod:NewCDTimer(15, 305264)

local Phase    = 1
local excludeTargets = {}

function mod:OnCombatStart(delay)
    Phase = 1
    if self:IsDifficulty("normal10") then
        timerCurseCD:Start()
    elseif self:IsDifficulty("heroic10") then
        self:PlaySound("po_konyam")
        timerInvCD:Start(20)
        self.combatInfo.killMobs = {[34973] = true}
        table.wipe(excludeTargets)
    end
end

function mod:ScanAggro()
    for i = 1, GetNumRaidMembers() do
        if UnitThreatSituation("raid" .. i) and UnitThreatSituation("raid" .. i) >= 2 then
            excludeTargets[UnitName("raid" .. i)] = true
        end
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(305258) then
        timerChargeCD:Start()
        if self:AntiSpam(200, "cena") then
            self:PlaySound("jhoncena")
        end
    elseif args:IsSpellID(305251) then
        warnInv:Show()
        timerInvCD:Start()
        table.wipe(excludeTargets)
        self:ScheduleMethod(1, "ScanAggro")
    elseif args:IsSpellID(305263) then 
        timerChargeP2CD:Start()
    elseif args:IsSpellID(305259) then
        timerSufferingCD:Start()
    elseif args:IsSpellID(305264) then
        timerTrampCD:Start()
        self:PlaySound("oh")
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(29833) then 
        warnCurse:Show()
        timerCurseCD:Start(Phase == 3 and 31)
        warnCurseSoon:Cancel()
        warnCurseSoon:Schedule(36)
    end
end

function mod:GhostTarget()
    local ghostTarget
    for i = 1, GetNumRaidMembers() do
        if UnitThreatSituation("raid" .. i) and UnitThreatSituation("raid" .. i) >= 2 and not excludeTargets[UnitName("raid" .. i) or ""] then
            ghostTarget = UnitName("raid" .. i)
            break
        end
    end
    table.wipe(excludeTargets)
    if not ghostTarget then return end
    warnGhost:Show(ghostTarget)
    if ghostTarget == UnitName("player") then
        specWarnGhost:Show()
        yellGhost:Yell()
    end
end

function mod:SPELL_SUMMON(args)
    if args:IsSpellID(305251) then
        self:ScheduleMethod(0.2, "GhostTarget")
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(305265) then
        timerChargeCD:Start()
        timerSufferingCD:Start()
        timerInvCD:Cancel()
        warnPhase2:Show()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(305265) then
        timerChargeP2CD:Start()
        timerTrampCD:Start(20)
        warnPhase3:Show()
    end
end

function mod:CHAT_MSG_MONSTER_EMOTE(msg)
    if msg == L.EmoteAttumen then
        warnAttumen:Show()
        self:PlaySound("idisuda")
    end
end

function mod:UNIT_HEALTH(uId)
    if (self:GetUnitCreatureId(uId) == 16151 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.3 and Phase == 1 and mod:IsDifficulty("normal10")) then
        self:PlaySound("po_konyam")
        Phase = 2
        warnAttumenSoon:Show()
    elseif (self:GetUnitCreatureId(uId) == 34971 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.52 and Phase == 1 and mod:IsDifficulty("heroic10")) then
        self:PlaySound("idisuda")
        Phase = 2
        warnPhase2Soon:Show()
    elseif (self:GetUnitCreatureId(uId) == 16152 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.5 and Phase == 2 and mod:IsDifficulty("normal10")) then
        Phase = 3
        timerCurseCD:Start(25)
        warnCurseSoon:Cancel()
        warnCurseSoon:Schedule(20)
    end
end
