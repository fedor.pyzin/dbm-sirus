local mod = DBM:NewMod("Netherspite", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4730 $"):sub(12, -3))
mod:SetCreatureID(15689)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_SUMMON",
    "SPELL_DAMAGE",
    "CHAT_MSG_RAID_BOSS_EMOTE"
)

local warningPortalSoon      = mod:NewAnnounce("DBM_NS_WARN_PORTAL_SOON", 2, "Interface\\Icons\\Spell_Arcane_PortalIronForge")
local warningBanishSoon      = mod:NewAnnounce("DBM_NS_WARN_BANISH_SOON", 2, "Interface\\Icons\\Spell_Shadow_Cripple")
local warningPortal          = mod:NewAnnounce("warningPortal", 3, "Interface\\Icons\\Spell_Arcane_PortalIronForge")
local warningBanish          = mod:NewAnnounce("warningBanish", 3, "Interface\\Icons\\Spell_Shadow_Cripple")
local warningBreathCast      = mod:NewCastAnnounce(38523, 2)
local warningVoid            = mod:NewSpellAnnounce(37063, 3)

local specWarnVoid           = mod:NewSpecialWarningMove(28865)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnGates              = mod:NewTargetAnnounce(305403, 3)

local specWarnGates          = mod:NewSpecialWarningYou(305403)

local timerPortalPhase       = mod:NewTimer(60, "timerPortalPhase", "Interface\\Icons\\Spell_Arcane_PortalIronForge")
local timerBanishPhase       = mod:NewTimer(30, "timerBanishPhase", "Interface\\Icons\\Spell_Shadow_Cripple")
local timerBreathCast        = mod:NewCastTimer(2.5, 38523)
local timerVoidCD            = mod:NewCDTimer(15, 37063)
local timerPortals           = mod:NewTimer(15, "TimerPortals" , 33404)
local timerNormalPhase       = mod:NewTimer(60, "TimerNormalPhase" ,23684)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerGates             = mod:NewNextTimer(13, 305400)
local timerGhostPhase        = mod:NewNextTimer(75, 305410)
local timerRepentance        = mod:NewBuffActiveTimer(30, 305408)
local timerBreatheCD         = mod:NewCDTimer(13, 305407)

local berserkTimer           = mod:NewBerserkTimer(540)

local gatesTargets = {}

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal10") then
        berserkTimer:Start(-delay)
        timerPortals:Start(-delay)
        timerVoidCD:Start(-delay)
        timerPortalPhase:Start(-delay)
        warningBanishSoon:Schedule(55-delay)
    elseif self:IsDifficulty("heroic10") then
        timerGates:Start()
        timerGhostPhase:Start()
        self:ScheduleMethod(75, "PlaySound", "run")
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(38523) then
        warningBreathCast:Show()
        timerBreathCast:Start()
    elseif args:IsSpellID(305407) then
        timerBreatheCD:Start()
    end
end

function mod:SPELL_SUMMON(args)
    if args:IsSpellID(37063) then
        warningVoid:Show()
        timerVoidCD:Start()
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(305402) then
        if args:IsPlayer() then
            specWarnGates:Show()
        end
        gatesTargets[#gatesTargets + 1] = args.destName
        if #gatesTargets >= 3 then
            warnGates:Show(table.concat(gatesTargets, "<, >"))
            table.wipe(gatesTargets)
        end
    elseif args:IsSpellID(305408, 305409) then
        self:ScheduleMethod(135, "PlaySound", "run")
        timerRepentance:Start()
        timerPortals:Start()
        timerNormalPhase:Start()
        timerGates:Schedule(60)
        timerGhostPhase:Schedule(60)
    elseif args:IsSpellID(305413) and args:IsPlayer() then
        self:PlaySound("buyniy")
    elseif args:IsSpellID(305415) and args:IsPlayer() then
        self:PlaySound("noice")
    elseif args:IsSpellID(305418) and args:IsPlayer() then
        self:PlaySound("swamp")
    end
end

function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(28865) and args:IsPlayer() and self:AntiSpam(2, "void") then
        specWarnVoid:Show()
        self:PlaySound("vnoge")
    end
end

function mod:CHAT_MSG_RAID_BOSS_EMOTE(msg)
    if msg == L.DBM_NS_EMOTE_PHASE_2 then
        self:PlaySound("run", "fear2")
        timerPortalPhase:Cancel()
        timerVoidCD:Cancel()
        warningBanish:Show()
        timerBanishPhase:Start()
        warningPortalSoon:Schedule(25)
    elseif msg == L.DBM_NS_EMOTE_PHASE_1 then
        timerBanishPhase:Cancel()
        warningPortal:Show()
        timerVoidCD:Start()
        timerPortalPhase:Start()
        warningBanishSoon:Schedule(55)
    end
end

function mod:OnCombatEnd()
    self:UnscheduleMethod("PlaySound", "run")
end
