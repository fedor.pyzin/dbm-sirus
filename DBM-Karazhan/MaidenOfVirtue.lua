local mod = DBM:NewMod("Maiden", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4750 $"):sub(12, -3))
mod:SetCreatureID(16457)
mod:SetUsedIcons(8)
mod:RegisterCombat("combat")
--mod:RegisterCombat("yell", L.YellPull)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_REMOVED",
    "CHAT_MSG_MONSTER_YELL",
    "SPELL_DAMAGE"
)

local warnRepentanceSoon       = mod:NewSoonAnnounce(29511, 2)
local warnRepentance           = mod:NewSpellAnnounce(29511, 3)
local warnHolyFire             = mod:NewTargetAnnounce(29522, 3)
mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnRepentanceSoonH      = mod:NewSoonAnnounce(305277, 2)
local warnRepentanceH          = mod:NewSpellAnnounce(305277, 4)
local warnGround               = mod:NewTargetAnnounce(305271, 3)
local warnHolyShock            = mod:NewSpellAnnounce(305276, 3)

local specWarnGround           = mod:NewSpecialWarningYou(305271)

local yellHolyGround           = mod:NewYell(305271)

local timerRepentance          = mod:NewBuffActiveTimer(12.6, 29511)
local timerRepentanceCD        = mod:NewCDTimer(37, 29511)
local timerHolyFire            = mod:NewTargetTimer(12, 29522)
local berserkTimer             = mod:NewBerserkTimer(600)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerRepentanceHCD       = mod:NewCDTimer(62, 305277)
local timerGroundCD            = mod:NewCDTimer(20, 305271)
local timerHolyShockCD         = mod:NewCDTimer(16, 305276)

mod:AddSetIconOption("SetIconOnGroundTarget", 305271, {8}, true)
mod:AddBoolOption("HealthFrame", true)
mod:AddBoolOption("RangeFrame", true)

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal10") then
        timerRepentanceCD:Start(-delay)
        warnRepentanceSoon:Schedule(32-delay)
        berserkTimer:Start(-delay)
        if self.Options.RangeFrame then
            DBM.RangeCheck:Show(12)
        end
    elseif self:IsDifficulty("heroic10") then
        warnRepentanceSoonH:Schedule(47-delay)
        timerRepentanceHCD:Start(52-delay)
        timerGroundCD:Start(-delay)
        timerHolyShockCD:Start(-delay)
        if self.Options.HealthFrame then
            DBM.BossHealth:Show(L.name)
        end
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellRepentence1 or msg == L.YellRepentence2 then
        timerRepentanceCD:Start()
        warnRepentanceSoon:Cancel()
        warnRepentanceSoon:Schedule(32)
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(29511) then
        self:PlaySound("pokaysya","greshnik")
        warnRepentance:Show()
        timerRepentance:Start()
    elseif args:IsSpellID(305277) then
        self:PlaySound("pokaysya","greshnik")
        warnRepentanceH:Show()
        warnRepentanceSoonH:Schedule(57)
        timerRepentanceHCD:Start()
    elseif args:IsSpellID(305276) then
        warnHolyShock:Show()
        timerHolyShockCD:Start()
    elseif args:IsSpellID(305286) then
        timerHolyShockCD:Start(21)
        timerGroundCD:Start(27)
    end
end

local showShieldHealthBar, hideShieldHealthBar, absorbed
do
    local frame = CreateFrame("Frame")
    local shieldedMob
    local absorbRemaining = 0
    local maxAbsorb = 0
    local function getShieldHP()
        return math.max(1, math.floor(absorbRemaining / maxAbsorb * 100))
    end
    frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    frame:SetScript("OnEvent", function(self, event, timestamp, subEvent, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, ...)
        if shieldedMob == destGUID then
            absorbed = nil
            if subEvent == "SWING_MISSED" then 
                absorbed = select( 2, ... ) 
            elseif subEvent == "RANGE_MISSED" or subEvent == "SPELL_MISSED" or subEvent == "SPELL_PERIODIC_MISSED" then 
                absorbed = select( 5, ... )
            end
            if absorbed then
                absorbRemaining = absorbRemaining - absorbed
            end
        end
    end)
    
    function showShieldHealthBar(self, mob, shieldName, absorb)
        shieldedMob = mob
        absorbRemaining = absorb
        maxAbsorb = absorb
        DBM.BossHealth:RemoveBoss(getShieldHP)
        DBM.BossHealth:AddBoss(getShieldHP, shieldName)
        self:Schedule(26, hideShieldHealthBar)
    end
    
    function hideShieldHealthBar()
        DBM.BossHealth:RemoveBoss(getShieldHP)
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(29522) then
        warnHolyFire:Show(args.destName)
        timerHolyFire:Start(args.destName)
    elseif args:IsSpellID(305271) then
        timerGroundCD:Start()
        if args:IsPlayer() then
            specWarnGround:Show()
            yellHolyGround:Yell()
            self:PlaySound("bochok")    -- Ярик! Бочок потiк.
        else
            warnGround:Show(args.destName)
        end
        if self.Options.SetIconOnGroundTarget then
            self:SetIcon(args.destName, 8, 5)
        end
    elseif args:IsSpellID(305285) then
        showShieldHealthBar(self, args.destGUID, args.spellName, 600000)
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(29522) then
        timerHolyFire:Cancel(args.destName)
    elseif args:IsSpellID(305285) then
        hideShieldHealthBar()
    end
end


function mod:SPELL_DAMAGE(args)
    if args:IsSpellID(305276) and self:AntiSpam(5, "damage") then
        self:PlaySound("dmg")
    end
end

function mod:OnCombatEnd()
    if self.Options.HealthFrame then
        DBM.BossHealth:Clear()
    end
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
end
