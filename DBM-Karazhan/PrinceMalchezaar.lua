local mod = DBM:NewMod("Prince", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4820 $"):sub(12, -3))
mod:SetCreatureID(15690)
mod:SetUsedIcons(7, 8)

mod:RegisterCombat("combat", 15690)
--mod:RegisterCombat("yell", L.YellPull)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "CHAT_MSG_MONSTER_YELL",
    "UNIT_HEALTH",
    "UNIT_DIED"
)

local warnNovaCast                = mod:NewCastAnnounce(30852, 3)
local warnEnfeeble                = mod:NewTargetAnnounce(30843, 3)
local warnInfernal                = mod:NewSpellAnnounce(37277, 2)
local warnHellfire                = mod:NewSpellAnnounce(30859, 3)

local specWarnEnfeeble            = mod:NewSpecialWarningYou(30843)
local specWarnAmpMagic            = mod:NewSpecialWarningYou(39095, mod:IsTank())

mod:AddAnnounceLine(DBM_HEROIC_MODE)
local warnNovaCastH               = mod:NewCastAnnounce(305425, 3)
local warnCorruption              = mod:NewTargetAnnounce(305429, 1)
local warnFlame                   = mod:NewTargetAnnounce(305433, 3)
local warnIceSpike                = mod:NewTargetAnnounce(305443, 3)
local warnCallofDead              = mod:NewTargetAnnounce(305447, 4)
local warnArcaneCleave            = mod:NewAnnounce("WarnArcaneCleave", 2, 305427, mod:IsTank())
local warnNextPhase               = mod:NewAnnounce("WarnNextPhase", 1)

local specWarnCorruption          = mod:NewSpecialWarningYou(305429)
local specWarnFlame               = mod:NewSpecialWarningYou(305433)
local specWarnIceSpike            = mod:NewSpecialWarningYou(305443)
local specWarnCallofDead          = mod:NewSpecialWarningYou(305447)

local yellFlame                   = mod:NewYell(305433)
local yellIceSpike                = mod:NewYell(305443)
local yellCallofDead              = mod:NewYell(305447)

local timerNovaCD                 = mod:NewCDTimer(30, 30852)
local timerEnfeeble               = mod:NewBuffActiveTimer(9, 30843)
local timerEnfeebleCD             = mod:NewCDTimer(30, 30843)
local timerInfernalCD             = mod:NewCDTimer(45, 37277)
local timerHellfire               = mod:NewCastTimer(12, 30859)
local timerAmpMagicCD             = mod:NewCDTimer(27, 39095)
mod:AddTimerLine(DBM_HEROIC_MODE)
local timerNovaHCD                = mod:NewCDTimer(12, 305425)
local timerCorruptionCD           = mod:NewCDTimer(20, 305429)
local timerCurseCD                = mod:NewCDTimer(20, 305435)
local timerFlameCD                = mod:NewCDTimer(30, 305433)
local timerIceSpikeCD             = mod:NewCDTimer(10, 305443)
local timerCallofDeadCD           = mod:NewCDTimer(10, 305447)
local timerArcaneCleaveCD         = mod:NewCDTimer(8, 305427, nil, mod:IsTank())
local berserkTimer                = mod:NewBerserkTimer(720)

local phaseNormal       = 0
local enfeebleTargets   = {}

local phaseHeroic       = 0
local flameIcons        = 8
local flameTargets      = {}
local corruptionTargets = {}

mod:AddSetIconOption("SetIconOnFlame", 305433, {8, 7}, true)
mod:AddSetIconOption("SetIconOnSpike", 305443, {6}, true)
mod:AddSetIconOption("SetIconOnCall",  305447, {5}, true)
mod:AddBoolOption("RemoveWeaponOnMindControl", not mod:IsTank() and not mod:IsHealer())
mod:AddBoolOption("RemoveDefBuffsOnMindControl", true)
mod:AddBoolOption("RangeFrame", true)
mod:AddBoolOption("RemoveHysteriaOnEnfeeble", true)

function mod:OnCombatStart(delay)
    if self:IsDifficulty("normal10") then
        timerEnfeebleCD:Start()
        timerNovaCD:Start(35)
        timerInfernalCD:Start(40)
        phaseNormal = 1
        table.wipe(enfeebleTargets)
    elseif self:IsDifficulty("heroic10") then
        timerNovaHCD:Start()
        timerCorruptionCD:Start()
        timerCurseCD:Start()
        timerArcaneCleaveCD:Start()
        berserkTimer:Start()
        phaseHeroic = 1
        flameIcons = 8
        table.wipe(flameTargets)
        table.wipe(corruptionTargets)
        if self.Options.RangeFrame then
            DBM.RangeCheck:Show(10)
        end
    end
end

function mod:SpikeTarget()
    local target = self:GetBossTarget(15690)
    if not target then return end
    if self.Options.SetIconOnSpike then
        self:SetIcon(target, 5)
    end
    warnIceSpike:Show(target)
    if target == UnitName("player") then
        specWarnIceSpike:Show()
        yellIceSpike:Yell()
    end
end

function mod:RemoveWeapon()
    if self:IsWeaponDependent() then
        PickupInventoryItem(16)
        PutItemInBackpack()
        PickupInventoryItem(17)
        PutItemInBackpack()
    elseif select(2, UnitClass("player")) == "HUNTER" then
        PickupInventoryItem(18)
        PutItemInBackpack()
    end
end

function mod:RemoveBuffs()
    CancelUnitBuff("player", (GetSpellInfo(48469)))        -- Mark of the Wild
    CancelUnitBuff("player", (GetSpellInfo(48470)))        -- Gift of the Wild
    CancelUnitBuff("player", (GetSpellInfo(48169)))        -- Shadow Protection
    CancelUnitBuff("player", (GetSpellInfo(48170)))        -- Prayer of Shadow Protection
    CancelUnitBuff("player", (GetSpellInfo(48707)))        -- Anti-Magic Shell
    CancelUnitBuff("player", (GetSpellInfo(49222)))        -- Bone Shield
    CancelUnitBuff("player", (GetSpellInfo(48792)))        -- Icebound Fortitude
    CancelUnitBuff("player", (GetSpellInfo(304822)))       -- Icebound Fortitude (T4)
    CancelUnitBuff("player", (GetSpellInfo(23920)))        -- Spell Reflection
    CancelUnitBuff("player", (GetSpellInfo(871)))          -- Shield Wall
    CancelUnitBuff("player", (GetSpellInfo(304687)))       -- Shield Wall (T4)
    CancelUnitBuff("player", (GetSpellInfo(642)))          -- Divine Shield
    CancelUnitBuff("player", (GetSpellInfo(498)))          -- Divine Protection
    CancelUnitBuff("player", (GetSpellInfo(64205)))        -- Divine Sacrifice
    CancelUnitBuff("player", (GetSpellInfo(47585)))        -- Dispersion
    CancelUnitBuff("player", (GetSpellInfo(19263)))        -- Deterrence
    CancelUnitBuff("player", (GetSpellInfo(25228)))        -- Soul Link
    CancelUnitBuff("player", (GetSpellInfo(47891)))        -- Shadow Ward
    CancelUnitBuff("player", (GetSpellInfo(45438)))        -- Ice Block
    CancelUnitBuff("player", (GetSpellInfo(43012)))        -- Frost Ward
    CancelUnitBuff("player", (GetSpellInfo(43010)))        -- Fire Ward
    CancelUnitBuff("player", (GetSpellInfo(43020)))        -- Mana Shield
    CancelUnitBuff("player", (GetSpellInfo(43039)))        -- Ice Barrier
end

function mod:CallTarget()
    local target = self:GetBossTarget(15690)
    if not target then return end
    if self.Options.SetIconOnCall then
        self:SetIcon(target, 5)
    end
    warnCallofDead:Show(target)
    if target == UnitName("player") then
        self:PlaySound("surprise")
        specWarnCallofDead:Show()
        yellCallofDead:Yell()
        if self.Options.RemoveWeaponOnMindControl then
            self:ScheduleMethod(3.5, "RemoveWeapon")
        end
        if self.Options.RemoveDefBuffsOnMindControl then
            self:ScheduleMethod(3.8, "RemoveBuffs")
        end
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(30852) then
        warnNovaCast:Show()
        timerNovaCD:Start(self:IsDifficulty("heroic10") and 13)
    elseif args:IsSpellID(305425) then
        warnNovaCastH:Show()
        timerNovaHCD:Start()
    elseif args:IsSpellID(305443) then
        timerIceSpikeCD:Start()
        self:ScheduleMethod(0.1, "SpikeTarget")
    elseif args:IsSpellID(305447) then
        timerCallofDeadCD:Start()
        self:ScheduleMethod(0.1, "CallTarget")
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(305427) then
        timerArcaneCleaveCD:Start()
    elseif args:IsSpellID(305429) then
        if phaseHeroic == 2 or phaseHeroic == 6 then
            timerCorruptionCD:Start(15)
        elseif phaseHeroic == 4 then
            timerCorruptionCD:Start(9)
        else
            timerCorruptionCD:Start()
        end
    elseif args:IsSpellID(305433) then
        timerFlameCD:Start(10)
    end
end

function mod:WarnEnfeeble()
    warnEnfeeble:Show(table.concat(enfeebleTargets, "<, >"))
    table.wipe(enfeebleTargets)
end

function mod:WarnCorruption()
    warnCorruption:Show(table.concat(corruptionTargets, "<, >"))
    table.wipe(corruptionTargets)
end

function mod:WarnFlame()
    warnFlame:Show(table.concat(flameTargets, "<, >"))
    table.wipe(flameTargets)
    flameIcons = 8
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(30843) then
        timerEnfeebleCD:Start()
        enfeebleTargets[#enfeebleTargets + 1] = args.destName
        if args:IsPlayer() then
            specWarnEnfeeble:Show()
            timerEnfeeble:Start()
            if self.Options.RemoveHysteriaOnEnfeeble then
                CancelUnitBuff("player", (GetSpellInfo(49016)))
            end
        end
        self:UnscheduleMethod("WarnEnfeeble")
        self:ScheduleMethod(0.1, "WarnEnfeeble")
    elseif args:IsSpellID(49016) and self.Options.RemoveHysteriaOnEnfeeble and UnitAura("player", (GetSpellInfo(30843))) then
        CancelUnitBuff("player", (GetSpellInfo(49016)))
    elseif args:IsSpellID(30859) and not args:IsDestTypePlayer() then
        if self:IsDifficulty("normal10") then
            warnHellfire:Show()
        end
    elseif args:IsSpellID(39095) then
        timerAmpMagicCD:Start()
        if args:IsPlayer() then
            specWarnAmpMagic:Show()
        end
    elseif args:IsSpellID(305429) then
        if args:IsPlayer() then
            self:PlaySound("s_tormozi")
            specWarnCorruption:Show()
        end
        corruptionTargets[#corruptionTargets + 1] = args.destName
        self:UnscheduleMethod("WarnCorruption")
        self:ScheduleMethod(0.1, "WarnCorruption")
    elseif args:IsSpellID(305435) then
        timerCurseCD:Start(phaseHeroic == 2 and 30)
    elseif args:IsSpellID(305433) then
        if args:IsPlayer() and phaseHeroic ~= 5 then
            specWarnFlame:Show()
            yellFlame:Yell()
            self:PlaySound("impruved", "t_4pokusheniya", "t_ubit", "t_zaminirovali")
        end
        if phaseHeroic == 3 then
            timerFlameCD:Start(10)
        else
            timerFlameCD:Start()
            timerFlameCD:Schedule(30)
        end
        flameTargets[#flameTargets + 1] = args.destName
        if self.Options.SetIconOnFlame then
            self:SetIcon(args.destName, flameIcons, 5)
        end
        flameIcons = flameIcons - 1
        self:UnscheduleMethod("WarnFlame")
        self:ScheduleMethod(0.1, "WarnFlame")
    elseif args:IsSpellID(305428) then
        warnArcaneCleave:Show(args.spellName, args.destName, 1)
    end
end

function mod:SPELL_AURA_APPLIED_DOSE(args)
    if args:IsSpellID(305428) then
        warnArcaneCleave:Show(args.spellName, args.destName, args.amount)
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellInfernal1 or msg == L.YellInfernal2 then
        warnInfernal:Show()
        timerHellfire:Start()
        timerInfernalCD:Start(phaseNormal == 3 and 17 or 45)
    elseif msg == L.YellPhase2 then
        phaseNormal = 2
        warnNextPhase:Show("2")
    elseif msg == L.YellPhase3 then
        phaseNormal = 3
        warnNextPhase:Show("3")
        timerAmpMagicCD:Start(5)
        timerNovaCD:Start(5)
        timerInfernalCD:Start(25)
    end
end

function mod:UNIT_HEALTH(uId)
    if phaseHeroic == 1 and self:GetUnitCreatureId(uId) == 15690 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.85 then
        phaseHeroic = phaseHeroic + 1
        warnNextPhase:Show("2")
        timerNovaHCD:Start()
        timerFlameCD:Start()
        timerCurseCD:Start(30)
        timerArcaneCleaveCD:Start()
    elseif phaseHeroic == 2 and self:GetUnitCreatureId(uId) == 15690 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.4 then
        phaseHeroic = phaseHeroic + 1
        warnNextPhase:Show("3: " .. L.FlameWorld)
        timerCurseCD:Cancel()
        timerNovaHCD:Cancel()
        timerCorruptionCD:Start(15)
        timerNovaCD:Start(13)
        timerFlameCD:Start(10)
        timerArcaneCleaveCD:Start()
        self:ScheduleMethod(2, "PlaySound", "kazakhstan")
    elseif phaseHeroic == 3 and self:GetUnitCreatureId(uId) == 15690 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.3 then
        phaseHeroic = phaseHeroic + 1
        warnNextPhase:Show("4: " .. L.IceWorld)
        timerFlameCD:Cancel()
        timerNovaCD:Cancel()
        timerCorruptionCD:Start(9)
        timerIceSpikeCD:Start()
        timerCurseCD:Start()
        timerArcaneCleaveCD:Start()
        self:ScheduleMethod(2, "PlaySound", "vholodilnike")
    elseif phaseHeroic == 4 and self:GetUnitCreatureId(uId) == 15690 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.2 then
        phaseHeroic = phaseHeroic + 1
        warnNextPhase:Show("5: " .. L.BlackForest)
        timerCurseCD:Cancel()
        timerIceSpikeCD:Cancel()
        timerCorruptionCD:Cancel()
        timerFlameCD:Start()
        timerCallofDeadCD:Start()
        timerArcaneCleaveCD:Start()
        self:ScheduleMethod(2, "PlaySound", "w_rice")
    elseif phaseHeroic == 5 and self:GetUnitCreatureId(uId) == 15690 and UnitHealth(uId) / UnitHealthMax(uId) <= 0.1 then
        phaseHeroic = phaseHeroic + 1
        warnNextPhase:Show("6: " .. L.LastPhase)
        timerCallofDeadCD:Cancel()
        timerNovaHCD:Start()
        timerCorruptionCD:Start(15)
        timerFlameCD:Start()
        timerArcaneCleaveCD:Start()
        self:ScheduleMethod(5, "PlaySound", "dobivaem")
    end
end

function mod:UNIT_DIED(args)
    local cid = self:GetCIDFromGUID(args.destGUID)
    if cid == 15690 and self:IsDifficulty("heroic10") then
        self:PlaySound("s_ya_vyigral")
    end
end

function mod:OnCombatEnd()
    phaseNormal = 0
    phaseHeroic = 0
    table.wipe(enfeebleTargets)
    table.wipe(flameTargets)
    table.wipe(corruptionTargets)
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
end
