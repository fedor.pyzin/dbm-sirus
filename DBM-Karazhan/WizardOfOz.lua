local mod = DBM:NewMod("Oz", "DBM-Karazhan")
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4500 $"):sub(12, -3))
mod:SetCreatureID(18168)
mod:RegisterCombat("yell", L.YellDorothee)
mod:SetMinCombatTime(25)

mod:RegisterEvents(
    "SPELL_CAST_START",
    "CHAT_MSG_MONSTER_YELL"
)

local WarnRoar           = mod:NewAnnounce("WarnRoar", 2, "Interface\\Icons\\Ability_Druid_ChallangingRoar", nil, false)
local WarnStrawman       = mod:NewAnnounce("WarnStrawman", 2, "Interface\\Icons\\INV_Helmet_34", nil, false)
local WarnTinhead        = mod:NewAnnounce("WarnTinhead", 2, "Interface\\Icons\\INV_Helmet_02", nil, false)
local WarnTido           = mod:NewAnnounce("WarnTito", 2, 31014, nil, false)
local WarnCrone          = mod:NewAnnounce("WarnCrone", 2, 1714, nil, false)

local timerCombatStart   = mod:NewTimer(72, "TimerCombatStart", 2457)
local timerRoar          = mod:NewTimer(5, "WarnRoar", "Interface\\Icons\\Ability_Druid_ChallangingRoar", nil, false)
local timerStrawman      = mod:NewTimer(15, "WarnStrawman", "Interface\\Icons\\INV_Helmet_34", nil, false)
local timerTinhead       = mod:NewTimer(25, "WarnTinhead", "Interface\\Icons\\INV_Helmet_02", nil, false)
local timerTito          = mod:NewTimer(35, "WarnTito", 31014, nil, false)

mod:AddBoolOption("AnnounceBosses", true, "announce")
mod:AddBoolOption("ShowBossTimers", true, "timer")
mod:AddBoolOption("OptionRangeFrameP2")

function mod:OnCombatStart(delay)
    if self.Options.ShowBossTimers then
        timerRoar:Start(-delay)
        timerStrawman:Start(-delay)
        timerTinhead:Start(-delay)
        timerTito:Start(-delay)
    end
end

function mod:OnCombatEnd()
    if self.Options.OptionRangeFrameP2 then
        DBM.RangeCheck:Hide()
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if msg == L.YellOzPrePull then
        timerCombatStart:Start()
    elseif msg == L.YellRoar then
        if self.Options.AnnounceBosses then
            WarnRoar:Show()
        end
    elseif msg == L.YellStrawman then
        if self.Options.AnnounceBosses then
            WarnStrawman:Show()
        end
    elseif msg == L.YellTinhead then
        if self.Options.AnnounceBosses then
            WarnTinhead:Show()
        end
    elseif msg == L.YellCrone then
        if self.Options.AnnounceBosses then
            WarnCrone:Show()
        end
        if self.Options.OptionRangeFrameP2 then
            DBM.RangeCheck:Show(10)
        end
    end
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(31014) then
        if self.Options.AnnounceBosses then
            WarnTido:Schedule(1)
        end
    end
end
