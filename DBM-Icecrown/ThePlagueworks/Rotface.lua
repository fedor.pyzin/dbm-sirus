local mod = DBM:NewMod("Rotface", "DBM-Icecrown", 2)
local L   = mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 4877 $"):sub(12, -3))
mod:SetCreatureID(36627)
mod:SetUsedIcons(7, 8)
mod:RegisterCombat("combat")

mod:RegisterEvents(
    "SPELL_CAST_START",
    "SPELL_AURA_APPLIED",
    "SPELL_AURA_APPLIED_DOSE",
    "SPELL_CAST_SUCCESS",
    "SPELL_AURA_REMOVED",
    "CHAT_MSG_MONSTER_YELL"
)

local InfectionIcon

local warnSlimeSpray            = mod:NewSpellAnnounce(69508, 2)
local warnMutatedInfection      = mod:NewTargetAnnounce(71224, 4)
local warnRadiatingOoze         = mod:NewSpellAnnounce(69760, 3)
local warnOozeSpawn             = mod:NewAnnounce("WarnOozeSpawn", 1)
local warnStickyOoze            = mod:NewSpellAnnounce(69774, 1)
local warnUnstableOoze          = mod:NewAnnounce("WarnUnstableOoze", 2, 69558)
local warnVileGas               = mod:NewTargetAnnounce(72272, 3)

local specWarnMutatedInfection  = mod:NewSpecialWarningYou(71224)
local specWarnStickyOoze        = mod:NewSpecialWarningMove(69774)
local specWarnOozeExplosion     = mod:NewSpecialWarningMove(69839)
local specWarnSlimeSpray        = mod:NewSpecialWarningSpell(69508, false)
local specWarnVileGas           = mod:NewSpecialWarningYou(72272)

local timerStickyOoze           = mod:NewNextTimer(16, 69774, nil, mod:IsTank())
local timerWallSlime            = mod:NewTimer(20, "NextPoisonSlimePipes", 69789)
local timerSlimeSpray           = mod:NewNextTimer(21, 69508)
local timerMutatedInfection     = mod:NewTargetTimer(12, 71224)
local timerOozeExplosion        = mod:NewCastTimer(4, 69839)
local timerVileGasCD            = mod:NewNextTimer(30, 72272)

mod:AddBoolOption("RangeFrame", mod:IsRanged())
mod:AddSetIconOption("InfectionIcon", 71224, {8,7}, true)

local RFVileGasTargets    = {}
local InfectionIcon = 8

function mod:OnCombatStart(delay)
    timerWallSlime:Start(25-delay)
    self:ScheduleMethod(25-delay, "WallSlime")
    InfectionIcon = 8
    if mod:IsDifficulty("heroic10") or mod:IsDifficulty("heroic25") then
        timerVileGasCD:Start(22-delay)
        if self.Options.RangeFrame then
            DBM.RangeCheck:Show(8)
        end
    end
end

function mod:OnCombatEnd()
    if self.Options.RangeFrame then
        DBM.RangeCheck:Hide()
    end
end

function mod:WallSlime()
    if self:IsInCombat() then
        timerWallSlime:Start()
        self:UnscheduleMethod("WallSlime")
        self:ScheduleMethod(20, "WallSlime")
    end
end

function mod:warnRFVileGasTargets()
    warnVileGas:Show(table.concat(RFVileGasTargets, "<, >"))
    table.wipe(RFVileGasTargets)
    timerVileGasCD:Start()
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(69508) then
        timerSlimeSpray:Start()
        warnSlimeSpray:Show()
        specWarnSlimeSpray:Show()
    elseif args:IsSpellID(69774) then        
        timerStickyOoze:Start()
        warnStickyOoze:Show()
    elseif args:IsSpellID(69839) and self:AntiSpam(10, "explosion") then
        timerOozeExplosion:Start()
        specWarnOozeExplosion:Schedule(4)
        self:ScheduleMethod(4, "PlaySound", "fireinthe")
    end
end

function mod:SPELL_AURA_APPLIED(args)
    if args:IsSpellID(71208) and args:IsPlayer() then
        specWarnStickyOoze:Show()
    elseif args:IsSpellID(69760) then
        warnRadiatingOoze:Show()
    elseif args:IsSpellID(69558) then
        warnUnstableOoze:Show(args.spellName, args.destName, args.amount or 1)
    elseif args:IsSpellID(69674, 71224, 73022, 73023) then
        warnMutatedInfection:Show(args.destName)
        timerMutatedInfection:Start(args.destName)
        if args:IsPlayer() then
            specWarnMutatedInfection:Show()
        end
        if self.Options.InfectionIcon then
            self:SetIcon(args.destName, InfectionIcon, 12)
            InfectionIcon = InfectionIcon == 8 and 7 or 8
        end
    elseif args:IsSpellID(72272, 72273) and args:IsDestTypePlayer() then
        RFVileGasTargets[#RFVileGasTargets + 1] = args.destName
        if args:IsPlayer() then
            specWarnVileGas:Show()
        end
        self:UnscheduleMethod("warnRFVileGasTargets")
        self:ScheduleMethod(2.5, "warnRFVileGasTargets")
    end
end

mod.SPELL_AURA_APPLIED_DOSE = mod.SPELL_AURA_APPLIED

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(72272, 72273) then
        timerVileGasCD:Start()
    end
end

function mod:SPELL_AURA_REMOVED(args)
    if args:IsSpellID(69674, 71224, 73022, 73023) then
        timerMutatedInfection:Cancel(args.destName)
        warnOozeSpawn:Show()
        if self.Options.InfectionIcon then
            self:SetIcon(args.destName, 0)
        end
    end
end

function mod:CHAT_MSG_MONSTER_YELL(msg)
    if (msg == L.YellSlimePipes1 or msg:find(L.YellSlimePipes1)) or (msg == L.YellSlimePipes2 or msg:find(L.YellSlimePipes2)) then
        self:WallSlime()
    end
end