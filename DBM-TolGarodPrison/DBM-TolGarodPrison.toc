﻿## Interface: 30300
## Title:|cffffe00a<|r|cffff7d0aDBM|r|cffffe00a>|r |cff69ccf0Tol'Garod Prison|r
## Title-ruRU:|cffffe00a<|r|cffff7d0aDBM|r|cffffe00a>|r |cff69ccf0Тол'Гародская тюрьма|r
## Author: Ritual
## LoadOnDemand: 1
## RequiredDeps: DBM-Core
## SavedVariablesPerCharacter: DBMTolGarodPrison_SavedVars, DBMTolGarodPrison_SavedStats
## X-DBM-Mod: 1
## X-DBM-Mod-Category: BC
## X-DBM-Mod-Name: Tol'Garod prison
## X-DBM-Mod-Name-ruRU: Тол'Гародская тюрьма
## X-DBM-Mod-Sort: 400
## X-DBM-Mod-LoadZone: Tol'Garod prison
## X-DBM-Mod-LoadZone-ruRU: Тол'Гародская тюрьма
localization.en.lua
localization.ru.lua
Gogonash.lua
Ktrax.lua
MagicDevourer.lua
